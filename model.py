import pandas as pd
import matplotlib.pyplot as plt
from math import exp
from scipy.optimize import minimize

df = pd.read_csv("dataset001.csv", delimiter=',')

x_set = []
y_set = []

for line in df.values:
	x_set.append(line[0])
	y_set.append(line[1])
	
plt.plot(x_set, y_set, '.')
plt.show()

# Model for curve-fitting
def y_calc(x,L):
	A, B, C, D, E, F = L
	
	return A*(x**B) + C + D*x + E*exp(x*F)

def min_fun(L):
	error = 0
	
	y_list = []		# Array containing final set of y terms
	
	i = 0
	
	#Getting the sum of squares of errors of the calculated and expected values
	while i < len(x_set):
		x = x_set[i]
		y = y_calc(x, L)
		y_list.append(y)
		
		error += (y_set[i] - y)**2
		
		i += 1
	
	# Calculating the RMS error of the loop
	error /= len(x_set)
	error = error**.5
	
	print(error)
	
	return error

# Initial guess of L
L = [1, 1, 1, 1, -1, -1]

#bounds
bounds_A = (None, None)
bounds_F = (0.0, 1.0)

bnds = (bounds_A, bounds_A, bounds_A, bounds_A, bounds_A, bounds_F)

result = minimize(min_fun, L, method='SLSQP', bounds=bnds)

print(result)